# encoding: UTF-8
require 'foghorn'

# A fake Fog module to test Foghorn without a cloud
module FakeFog
  # Compute
  class Compute
    def initialize(opts)
      @servers = opts[:servers]
      @snapshots = opts[:snapshots]
      @tags = opts[:tags]
    end
    
    def delete_snapshot(id)
      @snapshots.each do |snapshot|
        if snapshot.id == id
          snapshot.delete
          return
        end
      end
    end

    attr_reader :servers, :snapshots, :tags
  end
  
  class Snapshot
    attr_accessor :description, :volume_id, :id
    
    def save
    end
    
    def reload
    end
  end
end

RSpec.describe Foghorn do
  describe '#roles_for' do
    it 'returns empty for nil Roles' do
      obj = double('obj')
      allow(obj).to receive(:tags) { {} }
      expect(Foghorn.roles_for obj).to eq([])
    end
    it 'returns empty for blank Roles' do
      obj = double('obj')
      allow(obj).to receive(:tags) { { 'Roles' => '' } }
      expect(Foghorn.roles_for obj).to eq([])
    end
    it 'returns a single Role' do
      obj = double('obj')
      allow(obj).to receive(:tags) { { 'Roles' => 'server' } }
      expect(Foghorn.roles_for obj).to eq(%w(server))
    end
    it 'returns multiple Roles separated by spaces' do
      obj = double('obj')
      allow(obj).to receive(:tags) { { 'Roles' => 'server db cron' } }
      expect(Foghorn.roles_for obj).to eq(%w(server db cron))
    end
    it 'returns multiple Roles separated by commas' do
      obj = double('obj')
      allow(obj).to receive(:tags) { { 'Roles' => 'server, db, cron' } }
      expect(Foghorn.roles_for obj).to eq(%w(server db cron))
    end
  end

  describe 'Horn' do
    describe '#servers' do
      it 'returns empty with no servers' do
        stub_const('Fog', FakeFog)
        horn = Foghorn::Horn.new(
          'foghorn_config' => { servers: [] })

        expect(horn.servers).to eq([])
      end

      it 'lists servers' do
        app1 = double('app1')
        allow(app1).to receive(:tags) {
          { 'Name' => 'app-001', 'Roles' => 'app db' }
        }
        app2 = double('app2')
        allow(app2).to receive(:tags) {
          { 'Name' => 'app-002', 'Roles' => 'app' }
        }

        stub_const('Fog', FakeFog)
        horn = Foghorn::Horn.new(
          'foghorn_config' => { servers: [app1, app2] })

        expect(horn.servers).to eq([app1, app2])
      end

      it 'lists servers by roles' do
        app1 = double('app1')
        allow(app1).to receive(:tags) {
          { 'Name' => 'app-001', 'Roles' => 'app db' }
        }
        app2 = double('app2')
        allow(app2).to receive(:tags) {
          { 'Name' => 'app-002', 'Roles' => 'app' }
        }

        stub_const('Fog', FakeFog)
        horn = Foghorn::Horn.new(
          'foghorn_config' => { servers: [app1, app2] })

        expect(horn.servers('app')).to eq([app1, app2])
        expect(horn.servers('db')).to eq([app1])
        expect(horn.servers('cron')).to eq([])

        expect(horn.servers('app', 'db', 'cron')).to eq([app1, app2])
        expect(horn.servers('db', 'cron')).to eq([app1])
      end
    end

    describe '#safeguard' do
      it 'passes with no servers' do
        stub_const('Fog', FakeFog)
        horn = Foghorn::Horn.new(
          'foghorn_config' => { servers: [] })

        horn.safeguard
      end

      it 'passes when servers are properly tagged' do
        app1 = double('app1')
        allow(app1).to receive(:tags) {
          { 'Name' => 'app-001', 'Roles' => 'app db' }
        }
        app2 = double('app2')
        allow(app2).to receive(:tags) {
          { 'Name' => 'app-002', 'Roles' => 'app' }
        }

        stub_const('Fog', FakeFog)
        horn = Foghorn::Horn.new(
          'foghorn_config' => { servers: [app1, app2] })

        horn.safeguard
      end

      it 'fails when a server is not tagged' do
        app1 = double('app1')
        allow(app1).to receive(:tags) {
          { 'Name' => 'app-001', 'Roles' => 'app db' }
        }
        app2 = double('app2')
        allow(app2).to receive(:tags) {
          { 'Name' => 'app-002' }
        }

        stub_const('Fog', FakeFog)
        horn = Foghorn::Horn.new(
          'foghorn_config' => { servers: [app1, app2] })

        expect { horn.safeguard }.to raise_error
      end
    end
    
    describe '#snapshot' do
      it 'makes snapshots' do
        app1 = double('app1')
        allow(app1).to receive(:tags) {
          { 'Name' => 'app-001', 'Snapshot' => 'app' }
        }
        vol0 = double('volume')
        allow(vol0).to receive(:id) { 'sda' }
        allow(app1).to receive(:volumes) { [ vol0 ] }
        
        snaps = double('snaps')
        expect(snaps).to receive(:new){ FakeFog::Snapshot.new }.once
        
        tags = double('tags')
        expect(tags).to receive(:create).exactly(4).times

        stub_const('Fog', FakeFog)
        horn = Foghorn::Horn.new(
          'foghorn_config' => { servers: [app1], snapshots: snaps, tags: tags })
        horn.active_servers << app1
        
        horn.snapshots
      end
    end
    
    describe '#snapshots_cleanup' do
      it 'deletes old snapshots' do
        app1 = double('app1')
        allow(app1).to receive(:tags) {
          { 'Name' => 'app-001', 'Snapshot' => 'app' }
        }
        snap1 = double('snap1')
        allow(snap1).to receive(:id) { 'snap-1' }
        expect(snap1).to receive(:delete).once
        allow(snap1).to receive(:tags) {
          { 'Server' => 'app-001', 'Time' => 413}
        }
        snap2 = double('snap2')
        allow(snap2).to receive(:id) { 'snap-2' }
        allow(snap2).to receive(:tags) {
          { 'Server' => 'app-001', 'Time' => 1024}
        }
        snap3 = double('snap3')
        allow(snap3).to receive(:id) { 'snap-3' }
        allow(snap3).to receive(:tags) {
          { 'Server' => 'app-001', 'Time' => 612}
        }

        stub_const('Fog', FakeFog)
        horn = Foghorn::Horn.new(
          'foghorn_config' => {
            servers: [app1],
            snapshots: [snap1, snap2, snap3] })
        
        horn.snapshots_cleanup
      end
      
      it 'preserves irrelevant snapshots' do
        app1 = double('app1')
        allow(app1).to receive(:tags) {
          { 'Name' => 'app-001', 'Snapshot' => 'app' }
        }
        app2 = double('app2')
        allow(app2).to receive(:tags) {
          { 'Name' => 'app-002' }
        }
        snap1 = double('snap1')
        allow(snap1).to receive(:id) { 'snap-1' }
        allow(snap1).to receive(:tags) {
          { 'Server' => 'app-002', 'Time' => 413}
        }
        snap2 = double('snap2')
        allow(snap2).to receive(:id) { 'snap-2' }
        allow(snap2).to receive(:tags) {
          { 'Server' => 'app-002', 'Time' => 1024}
        }
        snap3 = double('snap3')
        allow(snap3).to receive(:id) { 'snap-3' }
        allow(snap3).to receive(:tags) {
          { 'Server' => 'app-002', 'Time' => 612}
        }

        stub_const('Fog', FakeFog)
        horn = Foghorn::Horn.new(
          'foghorn_config' => {
            servers: [app1, app2],
            snapshots: [snap1, snap2, snap3] })
        
        horn.snapshots_cleanup
      end
      
      it 'keeps 2 snapshots from each server' do
        app1 = double('app1')
        allow(app1).to receive(:tags) {
          { 'Name' => 'app-001', 'Snapshot' => 'app' }
        }
        api1 = double('app1')
        allow(api1).to receive(:tags) {
          { 'Name' => 'api-001', 'Snapshot' => 'api' }
        }
        snap1 = double('snap1')
        allow(snap1).to receive(:id) { 'snap-1' }
        allow(snap1).to receive(:tags) {
          { 'Server' => 'app-001', 'Time' => 413}
        }
        snap2 = double('snap2')
        allow(snap2).to receive(:id) { 'snap-2' }
        allow(snap2).to receive(:tags) {
          { 'Server' => 'api-001', 'Time' => 1024}
        }
        snap3 = double('snap3')
        allow(snap3).to receive(:id) { 'snap-3' }
        allow(snap3).to receive(:tags) {
          { 'Server' => 'api-001', 'Time' => 612}
        }

        stub_const('Fog', FakeFog)
        horn = Foghorn::Horn.new(
          'foghorn_config' => {
            servers: [app1, api1],
            snapshots: [snap1, snap2, snap3] })
        
        horn.snapshots_cleanup
      end
    end
  end
end
