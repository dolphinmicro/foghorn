# Foghorn

Deploy to the cloud without specifying specific servers

## Requirements

Currently only works with Capistrano 2.15, tested with Amazon

## Installation

Add this line to your application's Gemfile:

```ruby
    gem 'foghorn', '0.0.3', git: 'git@bitbucket.org:dolphinmicro/foghorn.git'
```

Require Foghorn in your deploy.rb

```ruby
    # this must go before loading stages!
    set :foghorn_config,
      provider:              'AWS',
      aws_access_key_id:     'rotpacxullos',
      aws_secret_access_key: '12345',
      region:                'us-west-2'
    require 'foghorn/capistrano'
```

## Usage

In your individual stages, you can now set roles based on the Roles tag in the cloud:
```ruby
    role_by_tag :app,       :staging_app
    role_by_tag :db,        :staging_db
```

You can test your configuration with foghorn:blow
```ruby
    bundle exec cap production foghorn:blow
```