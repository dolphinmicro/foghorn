# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'foghorn/version'

Gem::Specification.new do |spec|
  spec.name          = 'foghorn'
  spec.version       = Foghorn::VERSION
  spec.authors       = ['Chris Dollard']
  spec.email         = ['chrisdollard@dolphinmicro.com']
  spec.description   = %q(Deploy to the cloud)
  spec.summary       = %q(Tag roles in EC2, not in your deploy scripts)
  spec.homepage      = 'https://bitbucket.org/dolphinmicro/foghorn'
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split($INPUT_RECORD_SEPARATOR)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rb-readline'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'pry_debug'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rspec', '~> 3.0'

  spec.add_runtime_dependency 'fog', '>= 1.19'
end
