# encoding: UTF-8
require 'foghorn/version'
require 'fog'
require 'set'

# Foghorn
module Foghorn
  def self.roles_for(object)
    (object.tags['Roles'] || '').split(/\W+/)
  end

  # This is the class that interacts with fog
  class Horn
    def initialize(cxt)
      if cxt.respond_to? :[]
        @context = cxt
      else
        @context = ->(v) {cxt.fetch(v) }
      end
      @fog = Fog::Compute.new(@context['foghorn_config'])
      @active_servers = Set.new
    end

    attr_reader :active_servers, :context

    # Get list of servers, optionally filtered by a list of roles
    def servers(*roles)
      if @servers.nil?
        @servers = @fog.servers
        @servers_by_role = Hash.new { |h, k| h[k] = [] }
        @servers.each do |server|
          Foghorn.roles_for(server).each do |role|
            @servers_by_role[role] << server
          end
        end
      end
      if roles.empty?
        return @servers
      else
        roles = roles.flatten(1) if roles.length == 1
        return roles
          .map { |role| @servers_by_role[role.to_s] }
          .inject([], :|)
      end
    end

    # Safeguards against forgetting to tag a server
    def safeguard
      servers
        .reject { |server| server.tags['Roles'] }
        .each do |server|
          fail "Server #{server.tags['Name']} has no tagged Roles! "\
            'Add a Roles tag to this server before deploying.'
        end
    end

    # Creates snapshots of all active servers
    def snapshots
      active_servers.each do |server|
        next unless server.tags['Snapshot']

        server.volumes.each.with_index do |volume, index|
          date = Time.now.strftime('%Y%m%d-%H%M')

          name = "#{server.tags['Name']}-volume#{index}-#{date}"

          snapshot             = @fog.snapshots.new
          snapshot.description = name
          snapshot.volume_id   = volume.id

          #puts "Saving snapshot #{name}"
          snapshot.save
          snapshot.reload # to get id for tags

          {
            Name: name,
            Time: Time.now.to_i,
            Server: server.tags['Name'],
            Revision: context['current_revision']
          }.each do |key, value|
            @fog.tags.create resource_id: snapshot.id, key: key, value: value
          end
        end
      end
    end

    # Delete old snapshots - all but 2
    def snapshots_cleanup
      server_names = servers
        .select { |server| server.tags['Snapshot']}
        .map { |server| server.tags['Name'] }

      snaps_by_server = Hash.new { |h, k| h[k] = [] }
      @fog.snapshots
        .select { |snapshot| server_names.include? snapshot.tags['Server'] }
        .each do |snapshot|
          snaps_by_server[snapshot.tags['Server']] << snapshot
        end

      snaps_by_server.each do |_server, snapshots|
        snapshots.sort_by! do |snapshot|
          (snapshot.tags['Time'] || 0).to_i * -1
        end

        snapshots.shift 2

        snapshots.each do |snapshot|
          begin
            #puts "Deleting snapshot #{snapshot.tags['Name']}"
            @fog.delete_snapshot snapshot.id
          rescue => e
            puts e.message
          end
        end
      end
    end
  end
end
