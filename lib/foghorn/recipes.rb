# encoding: UTF-8
require 'foghorn'

Capistrano::Configuration.instance.load do

  def role_by_tag(r, *tags)
    servers = foghorn.horn.servers(tags)
      .select { |server| server.dns_name }
    fail "Couldn't find any servers tagged with #{tags}" if servers.empty?
    puts r
    servers.each do |server|
      puts "        #{server.tags['Name']}"
      foghorn.horn.active_servers << server
      role r, server.dns_name
    end
  end

  namespace :foghorn do
    def horn
      @foghorn ||= Foghorn::Horn.new(self)
    end

    desc 'Show roles for all servers'
    task :blow do
      horn.servers.each do |server|
        puts(
          ([server.tags['Name']] + Foghorn.roles_for(server))
          .map { |col| col.ljust(16) }
          .join)
      end
    end

    desc 'Ensure all servers have tags'
    task :safeguard do
      horn.safeguard
    end

    desc 'Snapshot all deployable servers tagged "snapshot"'
    task :snapshots do
      horn.snapshots
    end

    desc 'Clean up old snapshots'
    task :snapshots_cleanup do
      horn.snapshots_cleanup
    end
  end
end
