# encoding: UTF-8
require 'capistrano/version'

if defined?(Capistrano::VERSION) &&
   Gem::Version.new(Capistrano::VERSION).release >= Gem::Version.new('3.0.0')
  load File.expand_path File.join File.dirname(__FILE__),
                                  'tasks',
                                  'foghorn.rake'
else
  load File.expand_path File.join File.dirname(__FILE__),
                                  'recipes.rb'
end
